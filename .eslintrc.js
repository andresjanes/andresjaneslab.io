module.exports = {
    env: {
        node: true
    },
    extends: [
        'plugin:@typescript-eslint/eslint-recommended',
        'plugin:@typescript-eslint/recommended',
        'plugin:vue/vue3-recommended',
        'prettier'
    ],
    rules: {
        // override/add rules settings here, such as:
        // 'vue/no-unused-vars': 'error'
        '@typescript-eslint/no-var-requires': 0
    },
    parserOptions: {
        parser: '@typescript-eslint/parser'
    }
}
