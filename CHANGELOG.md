# [0.6.0](https://gitlab.com/andresjanes/andresjanes.gitlab.io/compare/v0.5.1...v0.6.0) (2022-08-17)

### Bug Fixes

-   compatibility css fix ([257b0a5](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/257b0a55f424707b4c936766b428b0efc6a02cd5))
-   include test dir in tsconfig ([ac5bdb0](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/ac5bdb0f215845612178f67edb875770fe41b561))
-   reference vitest types in vite config ([b786f04](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/b786f047052f4fa3ad74d68332d2c84f1c606f7d))
-   remove unused import ([937884a](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/937884ae39f901ea997099117b694248332c2dbc))
-   use correct indent size in release script ([3324d89](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/3324d897619ce099508a80ba8702edefa89bf27b))

### Features

-   add release script and generate changelogs ([e8aae71](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/e8aae718ad1a7066aae17927a894ec859fb57c5a))
-   add release script to package.json ([5eb9417](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/5eb941709c8520814cb42744cdce902854e4a88d))

## [0.5.1](https://gitlab.com/andresjanes/andresjanes.gitlab.io/compare/v0.5.0...v0.5.1) (2022-02-15)

### Bug Fixes

-   don't generate tailwind class names dynamically ([de16d69](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/de16d6930e54081f8aeb0c2c28a14629a9953263))

# [0.5.0](https://gitlab.com/andresjanes/andresjanes.gitlab.io/compare/v0.4.0...v0.5.0) (2022-02-15)

# [0.4.0](https://gitlab.com/andresjanes/andresjanes.gitlab.io/compare/v0.3.0...v0.4.0) (2022-02-14)

# [0.3.0](https://gitlab.com/andresjanes/andresjanes.gitlab.io/compare/v0.2.0...v0.3.0) (2022-02-14)

### Bug Fixes

-   remove unused import from store ([dedb5e0](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/dedb5e0aa87a4bc70107ba97cc118e854a33bb64))

### Features

-   add BaseChip unit test ([dd9e4af](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/dd9e4af826aedff0b2cb0cc316e6545f973a086b))
-   add BaseSheet unit test ([827fa22](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/827fa22908dee5cd99e42136dbb7cebcf312fd69))
-   add ProfileHeader unit test ([ae624d6](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/ae624d66ef50e8de41fd791bac1eb9b455516f90))
-   add unit test for BaseIcon ([2600fde](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/2600fdeca3227c393880d59c5bbe90deeece3c8a))
-   add unit test for BaseIconLabel ([b1254c2](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/b1254c2b2c11578e9bae60feebe561f9aac6c38c))
-   add unit test for BaseTooltipButton ([5119dc2](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/5119dc265777de6930664203defb7446970aa2c4))
-   add unit test for ExperienceSection ([fba3e61](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/fba3e61af54f17f69476be939d36d664bdfb88b6))
-   add unit test for PortfolioItem ([0524eec](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/0524eecc8b29d15690ec036c8164067177db3307))
-   ignore files in .gitignore when running Prettier format ([2d846dc](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/2d846dc6bcc59afd1356eca2f3651076e0173244))
-   use short dates for experience sections ([4b44631](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/4b4463190662318b594bdd420d175f2f99fd1b75))

### Reverts

-   Revert "refactor: remove types file as not used" ([eb68064](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/eb68064dc350f36553280d77b080788bc04b2477))

# [0.2.0](https://gitlab.com/andresjanes/andresjanes.gitlab.io/compare/v0.1.0...v0.2.0) (2022-02-10)

### Features

-   add cv pdf download ([2522afc](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/2522afcc36ce8fe24a8b8d3f1c6fd8a510b95ebf))
-   add gitlab link ([31700e2](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/31700e2966a48135c26c9662f9a318616e407f1e))
-   add portfolio section ([94f9ebe](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/94f9ebea59915029b911d8b8ed94e0e350473f11))
-   add styling for print ([74acfae](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/74acfae3906e80ffee1d6969ba22a4566178dcc3))
-   update html title and favicon ([21cf025](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/21cf0252c9bafb79499d9c27ef7db143d1f3c5f3))

# [0.1.0](https://gitlab.com/andresjanes/andresjanes.gitlab.io/compare/9c5e91641b4934de99e9adb92817a546ee23001e...v0.1.0) (2022-02-10)

### Bug Fixes

-   fix command to start dev server in README ([13b5224](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/13b522484ff2f01b71938c19a48cc83c6aa83492))
-   remove IDE folder that should be ignored ([9d0483d](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/9d0483ddd8586a0dc58cd810676378b0f0aa1261))
-   use string primitive instead of Wrapper object in DetailsItem type def ([24ab4c6](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/24ab4c627cc9702729a81e0df3fcedfe47c34055))

### Features

-   add BaseChip component ([74c3386](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/74c3386faa2a592128c51ee2ef37dbeec860b4be))
-   add BaseDetailsSection component ([01d45e9](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/01d45e91ace06b56f51c24e65b47736e69fa15e9))
-   add BaseIcon component ([11f60f1](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/11f60f13353030898a06f8b9998fc8a61e2e2d06))
-   add BaseIconLabel component ([5a72f4e](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/5a72f4e283ef11e10601b62ba55092aacab0b635))
-   add BaseSection component ([020c9cd](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/020c9cd8dfa6d057b51148a81f5688ed69811b76))
-   add BaseSheet component ([2f29d94](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/2f29d94deadc1c824b9fcdd1941c9b4aec71a379))
-   add BaseTooltipButton component ([ebe711c](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/ebe711c090766dd513892b953c3b94f5639e4b72))
-   add ExperienceSection component ([1547c85](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/1547c85205af56d95c27d4ca8c414902aeb58a70))
-   add InfoSection component ([280d244](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/280d24410d462c950a4c03984b716348ab6cd694))
-   add vuex store and state including types ([02afb44](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/02afb4421d9555aa70029c13c1f009ae76aa05f1))
-   initial commit ([9c5e916](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/9c5e91641b4934de99e9adb92817a546ee23001e))
-   remove HelloWorld component ([adf52a9](https://gitlab.com/andresjanes/andresjanes.gitlab.io/commit/adf52a9095cdd136ee5abae6dfcb5c154a659f7d))
